package com.yucaplan.eduardo.yucaplan.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.yucaplan.eduardo.yucaplan.R;

public class WhatActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what);

        findViewById(R.id.button4).setOnClickListener(WhatActivity.this);
        findViewById(R.id.button5).setOnClickListener(WhatActivity.this);
        findViewById(R.id.button6).setOnClickListener(WhatActivity.this);
        findViewById(R.id.button7).setOnClickListener(WhatActivity.this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.button4:
                Intent intent = new Intent(WhatActivity.this, NightLifeActivity.class);
                startActivity(intent);
                break;
            case R.id.button5:
                Intent intent2 = new Intent(WhatActivity.this, RestaurantActivity.class);
                startActivity(intent2);
                break;
            case R.id.button6:
                Intent intent3 = new Intent(WhatActivity.this, TourismActivity.class);
                startActivity(intent3);
                break;
            case R.id.button7:
                Intent intent4 = new Intent(WhatActivity.this, CultureActivity.class);
                startActivity(intent4);
                break;
        }
    }
}

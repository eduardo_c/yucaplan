package com.yucaplan.eduardo.yucaplan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;
import com.yucaplan.eduardo.yucaplan.R;

import java.util.List;

/**
 * Created by eduardo on 15/11/2016.
 */

public class PromosAdapter extends RecyclerView.Adapter<PromosAdapter.CustomViewHolder> {
    private List<ParseObject> mListPromos;
    private Context mContext;

    public PromosAdapter(Context ctx, List<ParseObject> data){
        mListPromos = data;
        mContext = ctx;
    }

    public ParseObject getItem(int position){
        return mListPromos.get(position);
    }

    public void setData(List<ParseObject> data){
        mListPromos = data;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.promo_row, null);
        PromosAdapter.CustomViewHolder viewHolder = new PromosAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        ParseObject object = mListPromos.get(position);
        ParseFile file = (ParseFile) object.get("ImgPromocion");
        String url = file.getUrl();

        if(!TextUtils.isEmpty(url))
            Picasso.with(mContext)
                    .load(url)
                    .into(holder.imageView);

        holder.textTitle.setText(object.getString("Nombre"));
        holder.textDescription.setText(object.getString("Descripcion"));
    }

    @Override
    public int getItemCount() {
        return (null != mListPromos ? mListPromos.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder{
        protected ImageView imageView;
        protected TextView textTitle;
        protected TextView textDescription;

        public CustomViewHolder(View view){
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.iv_promo);
            this.textTitle = (TextView) view.findViewById(R.id.tv_promo_title);
            this.textDescription = (TextView) view.findViewById(R.id.tv_promo_description);
        }
    }
}

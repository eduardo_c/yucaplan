package com.yucaplan.eduardo.yucaplan.utils;

/**
 * Created by eduardo on 11/11/2016.
 */

public class DetailsExtraDataKeys {
    public static String KEY_NAME = "name";
    public static String KEY_DESCRIPTION = "description";
    public static String KEY_INI_DAY = "ini_day";
    public static String KEY_END_DAY = "end_day";
    public static String KEY_INI_HOUR = "ini_hour";
    public static String KEY_END_HOUR = "end_hour";
    public static String KEY_ADDRESS = "address";
    public static String KEY_LAT = "lat";
    public static String KEY_LON = "lon";
    public static String KEY_IMG_URL = "img";
}

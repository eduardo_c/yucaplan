package com.yucaplan.eduardo.yucaplan.adapter;

import android.content.Context;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;
import com.yucaplan.eduardo.yucaplan.R;

import java.util.List;

/**
 * Created by eduardo on 08/11/2016.
 */

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.CustomViewHolder> {
    private List<ParseObject> placeItemList;
    private Context mContext;

    public PlacesAdapter(Context ctx, List<ParseObject> list){
        placeItemList = list;
        mContext = ctx;
    }

    public ParseObject getItem(int position){
        return placeItemList.get(position);
    }

    public void setData(List<ParseObject> data){
        placeItemList = data;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.establecimiento_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        ParseObject item = placeItemList.get(position);
        ParseFile file = (ParseFile) item.get("ImgEstablecimiento");

        if(!TextUtils.isEmpty(file.getUrl())){
            Picasso.with(mContext).load(file.getUrl())
                    .placeholder(R.color.button_color)
                    .into(holder.imageView);
        }

        holder.textView.setText(item.getString("Nombre"));
    }

    @Override
    public int getItemCount() {
        return (null != placeItemList ? placeItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder{
        protected ImageView imageView;
        protected TextView textView;

        public CustomViewHolder(View view){
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.iv_thumb);
            this.textView = (TextView) view.findViewById(R.id.tv_place_name);
        }
    }
}

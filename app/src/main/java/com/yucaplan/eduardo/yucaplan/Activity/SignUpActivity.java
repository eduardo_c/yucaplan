package com.yucaplan.eduardo.yucaplan.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.yucaplan.eduardo.yucaplan.R;
import com.yucaplan.eduardo.yucaplan.preferences.SharedPreferencesManager;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private final Context mContext = SignUpActivity.this;
    private EditText mNameEditText;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mPassConfirmEditText;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        topLevelFocus(mContext);

        mProgressDialog = new ProgressDialog(mContext);
        findViewById(R.id.btn_signup).setOnClickListener(SignUpActivity.this);
        mNameEditText = (EditText) findViewById(R.id.et_name);
        mEmailEditText = (EditText) findViewById(R.id.et_mail);
        mPasswordEditText = (EditText) findViewById(R.id.et_password);
        mPassConfirmEditText = (EditText) findViewById(R.id.et_pass_confirm);

        mNameEditText.requestFocus();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_signup){
            mProgressDialog.setMessage(getString(R.string.signing_up));
            mProgressDialog.show();

            final String name = mNameEditText.getText().toString();
            String email = mEmailEditText.getText().toString();
            final String password = mPasswordEditText.getText().toString();
            String passConfirm = mPassConfirmEditText.getText().toString();

            if(password.equals(passConfirm)){
                ParseUser user = new ParseUser();
                user.setUsername(name);
                user.setEmail(email);
                user.setPassword(password);

                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        mProgressDialog.dismiss();
                        if(e == null){
                            Toast.makeText(mContext, getString(R.string.signup_success), Toast.LENGTH_LONG).show();
                            attemptLogIn(name, password);
                        }
                        else {
                            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
            else{
                mProgressDialog.dismiss();
                Toast.makeText(mContext, getString(R.string.error_password_confirm), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void attemptLogIn(String userName, String password){
        ParseUser.logInInBackground(userName, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if(parseUser != null){
                    SharedPreferencesManager.setUserEmail(mContext, parseUser.getEmail());
                    SharedPreferencesManager.setUserName(mContext, parseUser.getUsername());
                    SharedPreferencesManager.setUserId(mContext, parseUser.getObjectId());
                    setResult(3);
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    SignUpActivity.this.finish();
                }
                else {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public static void topLevelFocus(Context context){
        if(Activity.class.isAssignableFrom(context.getClass())){
            ViewGroup tlView = (ViewGroup) ((Activity) context).getWindow().getDecorView();
            if(tlView!=null){
                tlView.setFocusable(true);
                tlView.setFocusableInTouchMode(true);
                tlView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
            }
        }
    }

    @Override
    protected void onStop() {
        setResult(3);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        setResult(3);
        super.onDestroy();
    }
}

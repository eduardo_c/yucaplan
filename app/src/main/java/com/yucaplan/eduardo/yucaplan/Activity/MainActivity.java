package com.yucaplan.eduardo.yucaplan.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yucaplan.eduardo.yucaplan.R;
import com.yucaplan.eduardo.yucaplan.adapter.PlacesAdapter;
import com.yucaplan.eduardo.yucaplan.listener.RecyclerItemClickListener;
import com.yucaplan.eduardo.yucaplan.preferences.SharedPreferencesManager;
import com.yucaplan.eduardo.yucaplan.utils.DetailsExtraDataKeys;
import com.yucaplan.eduardo.yucaplan.utils.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import jp.wasabeef.picasso.transformations.BlurTransformation;

import static android.R.id.list;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private final String LOG_TAG = MainActivity.class.getSimpleName();
    private Context mContext = MainActivity.this;
    private NavigationView mNavigationView;
    private RecyclerView mRecyclerView;
    private List<ParseObject> mPlaceItems;
    private PlacesAdapter mAdapter;
    private ProgressBar mProgressBar;
    private ProgressDialog mProgressDialog;
    private LogOutTask mLogOutTask = null;
    private Dialog dialogWhen;
    private Dialog dialogWhere;
    private Dialog dialogWhat;
    private Dialog dialogNightLife;
    private Dialog dialogRestaurants;
    private Dialog dialogTourism;
    private Dialog dialogCulture;
    private TextView emptyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!SessionManager.isUserLogged(MainActivity.this)){
            Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.activity_main2);

        mProgressDialog = new ProgressDialog(mContext);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mNavigationView.setCheckedItem(R.id.nav_camara);

        TextView tv_username = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.nav_tv_username);
        tv_username.setText(SharedPreferencesManager.getUserName(mContext));

        TextView tv_mail = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.nav_tv_mail);
        tv_mail.setText(SharedPreferencesManager.getUserEmail(mContext));

        ImageView ivProfile = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.nav_iv_profile);

        Picasso.with(mContext)
                .load(SharedPreferencesManager.getUserProfilePicture(mContext))
                .into(ivProfile);

        final LinearLayout ll = (LinearLayout) mNavigationView.getHeaderView(0).findViewById(R.id.nav_linear_layout);
        final Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.d(LOG_TAG, "BITMAP LOADED ON CREATE");
                ll.setBackground(new BitmapDrawable(mContext.getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.d(LOG_TAG, "BITMAP FAILED ON CREATE");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d(LOG_TAG, "BITMAP PREPARE LOAD ON CREATE");
            }
        };
        ll.setTag(target);

        Picasso.with(mContext)
                .load(SharedPreferencesManager.getUserProfilePicture(mContext))
                .transform(new BlurTransformation(mContext, 15))
                .into(target);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        emptyTextView = (TextView) findViewById(R.id.tv_empty);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Establecimiento");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                mProgressBar.setVisibility(View.GONE);
                if(e == null){
                    if(list.isEmpty()) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mPlaceItems = list;
                        mAdapter = new PlacesAdapter(MainActivity.this, mPlaceItems);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                }
                else{
                    emptyTextView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Error al cargar establecimientos", Toast.LENGTH_LONG).show();
                }
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemCLickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ParseObject object = mAdapter.getItem(position);
                        ParseGeoPoint point = object.getParseGeoPoint("Ubicacion");
                        double lat = point.getLatitude();
                        double lon = point.getLongitude();
                        ParseFile file = (ParseFile) object.get("ImgEstablecimiento");

                        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);

                        intent.putExtra(DetailsExtraDataKeys.KEY_NAME, object.getString("Nombre"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_DESCRIPTION, object.getString("Descripcion"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_INI_DAY, object.getInt("DIni"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_END_DAY, object.getInt("DFin"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_INI_HOUR, object.getString("HIni"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_END_HOUR, object.getString("HFin"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_ADDRESS, object.getString("Direccion"));
                        intent.putExtra(DetailsExtraDataKeys.KEY_LAT, lat);
                        intent.putExtra(DetailsExtraDataKeys.KEY_LON, lon);
                        if(!TextUtils.isEmpty(file.getUrl()))
                            intent.putExtra(DetailsExtraDataKeys.KEY_IMG_URL, file.getUrl());
                        startActivity(intent);
                    }
                }));

        Button btnWhen = (Button) findViewById(R.id.tb_btn_when);
        btnWhen.setOnClickListener(MainActivity.this);
        Button btnWhere = (Button) findViewById(R.id.tb_btn_where);
        btnWhere.setOnClickListener(MainActivity.this);
        Button btnWhat = (Button) findViewById(R.id.tb_btn_what);
        btnWhat.setOnClickListener(MainActivity.this);

        dialogWhen = new Dialog(mContext);
        dialogWhen.setContentView(R.layout.content_main2);
        if (dialogWhen.getWindow() != null)
            dialogWhen.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogWhere = new Dialog(mContext);
        dialogWhere.setContentView(R.layout.activity_where);
        if(dialogWhere.getWindow() != null)
            dialogWhere.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogWhat = new Dialog(mContext);
        dialogWhat.setContentView(R.layout.activity_what);
        if(dialogWhat.getWindow() != null)
            dialogWhat.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogNightLife = new Dialog(mContext);
        dialogNightLife.setContentView(R.layout.activity_night_life);
        if(dialogNightLife.getWindow() != null)
            dialogNightLife.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogRestaurants = new Dialog(mContext);
        dialogRestaurants.setContentView(R.layout.activity_restaurant);
        if(dialogRestaurants.getWindow() != null)
            dialogRestaurants.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogTourism = new Dialog(mContext);
        dialogTourism.setContentView(R.layout.activity_tourism);
        if(dialogTourism.getWindow() != null)
            dialogTourism.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogCulture = new Dialog(mContext);
        dialogCulture.setContentView(R.layout.activity_culture);
        if(dialogCulture.getWindow() != null)
            dialogCulture.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogWhen.findViewById(R.id.btn_today).setOnClickListener(MainActivity.this);
        dialogWhen.findViewById(R.id.btn_tomorrow).setOnClickListener(MainActivity.this);
        dialogWhen.findViewById(R.id.btn_month).setOnClickListener(MainActivity.this);

        dialogWhere.findViewById(R.id.btn_north).setOnClickListener(MainActivity.this);
        dialogWhere.findViewById(R.id.btn_south).setOnClickListener(MainActivity.this);
        dialogWhere.findViewById(R.id.btn_east).setOnClickListener(MainActivity.this);
        dialogWhere.findViewById(R.id.btn_west).setOnClickListener(MainActivity.this);
        dialogWhere.findViewById(R.id.btn_center).setOnClickListener(MainActivity.this);

        dialogWhat.findViewById(R.id.button4).setOnClickListener(MainActivity.this);
        dialogWhat.findViewById(R.id.button5).setOnClickListener(MainActivity.this);
        dialogWhat.findViewById(R.id.button6).setOnClickListener(MainActivity.this);
        dialogWhat.findViewById(R.id.button7).setOnClickListener(MainActivity.this);

        dialogNightLife.findViewById(R.id.btn_pubs).setOnClickListener(MainActivity.this);
        dialogNightLife.findViewById(R.id.btn_clubs).setOnClickListener(MainActivity.this);
        dialogNightLife.findViewById(R.id.btn_cantinas).setOnClickListener(MainActivity.this);

        dialogRestaurants.findViewById(R.id.btn_regional).setOnClickListener(MainActivity.this);
        dialogRestaurants.findViewById(R.id.btn_mexican).setOnClickListener(MainActivity.this);
        dialogRestaurants.findViewById(R.id.btn_seafood).setOnClickListener(MainActivity.this);
        dialogRestaurants.findViewById(R.id.btn_international).setOnClickListener(MainActivity.this);

        dialogTourism.findViewById(R.id.btn_parks).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_haciendas).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_tours).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_buys).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_casinos).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_spas).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_hotels).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_interest).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_souvenirs).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_beachs).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_cenotes).setOnClickListener(MainActivity.this);
        dialogTourism.findViewById(R.id.btn_grutas).setOnClickListener(MainActivity.this);

        dialogCulture.findViewById(R.id.btn_museums).setOnClickListener(MainActivity.this);
        dialogCulture.findViewById(R.id.btn_theaters).setOnClickListener(MainActivity.this);
        dialogCulture.findViewById(R.id.btn_concerts).setOnClickListener(MainActivity.this);
        //dialogCulture.findViewById(R.id.btn_bicycle).setOnClickListener(MainActivity.this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings:
                return true;
            case R.id.action_logout:
                attemptLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void attemptLogOut(){
        if(mLogOutTask != null)
            return;

        mLogOutTask = new LogOutTask();
        mLogOutTask.execute();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.tb_btn_when:
                dialogWhen.show();
                break;
            case R.id.tb_btn_where:
                dialogWhere.show();
                break;
            case R.id.tb_btn_what:
                dialogWhat.show();
                break;
            case R.id.btn_today:
                dialogWhen.dismiss();
                executeTodayQuery(getDayOfWeek());
                break;
            case R.id.btn_tomorrow:
                dialogWhen.dismiss();
                executeTodayQuery(getTomorrowDayOfWeek());
                break;
            case R.id.btn_month:
                dialogWhen.dismiss();
                executeGetAllPlaces();
                break;
            case R.id.btn_north:
                dialogWhere.dismiss();
                executeQuery("IdZona", 1);
                break;
            case R.id.btn_south:
                dialogWhere.dismiss();
                executeQuery("IdZona", 2);
                break;
            case R.id.btn_east:
                dialogWhere.dismiss();
                executeQuery("IdZona", 3);
                break;
            case R.id.btn_west:
                dialogWhere.dismiss();
                executeQuery("IdZona", 4);
                break;
            case R.id.btn_center:
                dialogWhere.dismiss();
                executeQuery("IdZona", 5);
                break;
            case R.id.button4:
                dialogWhat.dismiss();
                /*List<Integer> types = new ArrayList<Integer>();
                types.add(1);
                types.add(2);
                types.add(3);
                executeTypePlaces(types);*/
                dialogNightLife.show();
                break;
            case R.id.button5:
                dialogWhat.dismiss();
                dialogRestaurants.show();
                break;
            case R.id.button6:
                dialogWhat.dismiss();
                dialogTourism.show();
                break;
            case R.id.button7:
                dialogWhat.dismiss();
                dialogCulture.show();
                break;
            case R.id.btn_pubs:
                dialogNightLife.dismiss();
                executeQuery("IdCategoria", 1);
                break;
            case R.id.btn_clubs:
                dialogNightLife.dismiss();
                executeQuery("IdCategoria", 2);
                break;
            case R.id.btn_cantinas:
                dialogNightLife.dismiss();
                executeQuery("IdCategoria", 3);
                break;
            case R.id.btn_regional:
                dialogRestaurants.dismiss();
                executeQuery("IdCategoria", 4);
                break;
            case R.id.btn_mexican:
                dialogRestaurants.dismiss();
                executeQuery("IdCategoria", 5);
                break;
            case R.id.btn_seafood:
                dialogRestaurants.dismiss();
                executeQuery("IdCategoria", 6);
                break;
            case R.id.btn_international:
                dialogRestaurants.dismiss();
                executeQuery("IdCategoria", 7);
                break;
            case R.id.btn_parks:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 8);
                break;
            case R.id.btn_haciendas:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 9);
                break;
            case R.id.btn_tours:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 10);
                break;
            case R.id.btn_buys:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 11);
                break;
            case R.id.btn_casinos:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 12);
                break;
            case R.id.btn_spas:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 13);
                break;
            case R.id.btn_hotels:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 14);
                break;
            case R.id.btn_interest:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 15);
                break;
            case R.id.btn_souvenirs:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 16);
                break;
            case R.id.btn_beachs:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 17);
                break;
            case R.id.btn_cenotes:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 18);
                break;
            case R.id.btn_grutas:
                dialogTourism.dismiss();
                executeQuery("IdCategoria", 19);
                break;
            case R.id.btn_museums:
                dialogCulture.dismiss();
                executeQuery("IdCategoria", 20);
                break;
            case R.id.btn_theaters:
                dialogCulture.dismiss();
                executeQuery("IdCategoria", 22);
                break;
            case R.id.btn_concerts:
                dialogCulture.dismiss();
                executeQuery("IdCategoria", 21);
                break;
        }
    }

    private class LogOutTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            mProgressDialog.setMessage(getString(R.string.logging_out));
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SessionManager.logout(mContext, ParseUser.getCurrentUser());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mLogOutTask = null;
            mProgressDialog.dismiss();
            Intent intent = new Intent(mContext, WelcomeActivity.class);
            startActivity(intent);
            MainActivity activity = (MainActivity) mContext;
            activity.finish();
        }
    }

    public void executeQuery(String column, int value){
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Establecimiento");
        query.whereEqualTo(column, value);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                mProgressBar.setVisibility(View.GONE);
                if(e == null){
                    if(list.isEmpty()){
                        emptyTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setData(list);
                        mAdapter.notifyDataSetChanged();
                    }
                }
                else{
                    emptyTextView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    Log.e(LOG_TAG, e.getMessage());
                    Toast.makeText(mContext, "Error cargando lugares", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void executeTodayQuery(int day){
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Establecimiento");
        query.whereGreaterThanOrEqualTo("DFin", day);
        query.whereLessThanOrEqualTo("DIni", day);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                mProgressBar.setVisibility(View.GONE);
                if(e == null){
                    if (list.isEmpty()) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    else{
                        emptyTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setData(list);
                        mAdapter.notifyDataSetChanged();
                    }
                }
                else{
                    emptyTextView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    Log.e(LOG_TAG, e.getMessage());
                    Toast.makeText(mContext, "Error cargando lugares", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void executeGetAllPlaces(){
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Establecimiento");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                mProgressBar.setVisibility(View.GONE);
                if(e == null){
                    if (list.isEmpty()) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    else{
                        emptyTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setData(list);
                        mAdapter.notifyDataSetChanged();
                    }
                }
                else{
                    emptyTextView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    Log.e(LOG_TAG, e.getMessage());
                    Toast.makeText(mContext, "Error cargando lugares", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void executeTypePlaces(List<Integer> types){
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Establecimiento");
        query.whereContainedIn("IdCategoria", types);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    mAdapter.setData(list);
                    mAdapter.notifyDataSetChanged();
                }
                mProgressBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    public int getDayOfWeek(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day){
            case Calendar.MONDAY:
                return 1;
            case Calendar.TUESDAY:
                return 2;
            case Calendar.WEDNESDAY:
                return 3;
            case Calendar.THURSDAY:
                return 4;
            case Calendar.FRIDAY:
                return 5;
            case Calendar.SATURDAY:
                return 6;
            case Calendar.SUNDAY:
                return 7;
            default:
                return 1;
        }
    }

    public int getTomorrowDayOfWeek(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day){
            case Calendar.MONDAY:
                return 2;
            case Calendar.TUESDAY:
                return 3;
            case Calendar.WEDNESDAY:
                return 4;
            case Calendar.THURSDAY:
                return 5;
            case Calendar.FRIDAY:
                return 6;
            case Calendar.SATURDAY:
                return 7;
            case Calendar.SUNDAY:
                return 1;
            default:
                return 1;
        }
    }
}

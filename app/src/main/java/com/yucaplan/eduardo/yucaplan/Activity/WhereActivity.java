package com.yucaplan.eduardo.yucaplan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.yucaplan.eduardo.yucaplan.R;

public class WhereActivity extends AppCompatActivity implements View.OnClickListener{

    private final Context mContext = WhereActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_where);
        findViewById(R.id.btn_north).setOnClickListener((View.OnClickListener) mContext);
        findViewById(R.id.btn_south).setOnClickListener((View.OnClickListener) mContext);
        findViewById(R.id.btn_east).setOnClickListener((View.OnClickListener) mContext);
        findViewById(R.id.btn_west).setOnClickListener((View.OnClickListener) mContext);
        findViewById(R.id.btn_center).setOnClickListener((View.OnClickListener) mContext);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mContext, WhatActivity.class);
        startActivity(intent);
    }
}

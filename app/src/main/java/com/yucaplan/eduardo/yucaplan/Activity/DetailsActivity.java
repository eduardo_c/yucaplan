package com.yucaplan.eduardo.yucaplan.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.drive.query.Query;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;
import com.yucaplan.eduardo.yucaplan.R;
import com.yucaplan.eduardo.yucaplan.adapter.PromosAdapter;
import com.yucaplan.eduardo.yucaplan.utils.DetailsExtraDataKeys;

import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class DetailsActivity extends AppCompatActivity implements OnMapReadyCallback{

    private final Context mContext = DetailsActivity.this;
    private final String LOG_TAG = DetailsActivity.class.getSimpleName();

    private TextView tvDescription;
    private TextView tvSchedule;
    private TextView tvAddress;
    private ImageView ivPlace;
    private RecyclerView rvPromos;
    private PromosAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        tvDescription = (TextView) findViewById(R.id.tv_description);
        tvSchedule = (TextView) findViewById(R.id.tv_schedule);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        ivPlace = (ImageView) findViewById(R.id.iv_place);
        rvPromos = (RecyclerView) findViewById(R.id.recycler_view_promos);
        rvPromos.setHasFixedSize(true);
        rvPromos.setLayoutManager(new LinearLayoutManager(DetailsActivity.this));

        String url = getIntent().getStringExtra(DetailsExtraDataKeys.KEY_IMG_URL);

        if(url != null && !TextUtils.isEmpty(url))
            Picasso.with(mContext)
                    .load(url)
                    .into(ivPlace);

        setTextValues();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("PromEstablecimiento");
        query.whereEqualTo("NomEstablecimiento", getIntent().getStringExtra(DetailsExtraDataKeys.KEY_NAME));
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null ){
                    if(list != null) {
                        mAdapter = new PromosAdapter(mContext, list);
                        rvPromos.setAdapter(mAdapter);
                        if(!list.isEmpty())
                            findViewById(R.id.tv_promos).setVisibility(View.VISIBLE);
                    }
                }
                else {
                    Toast.makeText(mContext, "Error cargando las promociones", Toast.LENGTH_LONG).show();
                }
            }
        });

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(DetailsActivity.this);
    }

    private void setTextValues(){
        String dayIni;
        String dayEnd;

        Intent intent = getIntent();
        if(getSupportActionBar() != null)
            getSupportActionBar().setTitle(intent.getStringExtra(DetailsExtraDataKeys.KEY_NAME));
        tvDescription.setText(intent.getStringExtra(DetailsExtraDataKeys.KEY_DESCRIPTION));

        switch (intent.getIntExtra(DetailsExtraDataKeys.KEY_INI_DAY, 1)){
            case 1:
                dayIni = "LUN";
                break;
            case 2:
                dayIni = "MAR";
                break;
            case 3:
                dayIni = "MIE";
                break;
            case 4:
                dayIni = "JUE";
                break;
            case 5:
                dayIni = "VIE";
                break;
            case 6:
                dayIni = "SAB";
                break;
            case 7:
                dayIni = "DOM";
                break;
            default:
                dayIni = "LUN";
                break;
        }

        switch (intent.getIntExtra(DetailsExtraDataKeys.KEY_END_DAY, 1)){
            case 1:
                dayEnd = "LUN";
                break;
            case 2:
                dayEnd = "MAR";
                break;
            case 3:
                dayEnd = "MIE";
                break;
            case 4:
                dayEnd = "JUE";
                break;
            case 5:
                dayEnd = "VIE";
                break;
            case 6:
                dayEnd = "SAB";
                break;
            case 7:
                dayEnd = "DOM";
                break;
            default:
                dayEnd = "LUN";
                break;
        }

        String hIni = intent.getStringExtra(DetailsExtraDataKeys.KEY_INI_HOUR);
        String hEnd = intent.getStringExtra(DetailsExtraDataKeys.KEY_END_HOUR);
        tvSchedule.setText(dayIni + " - " + dayEnd + " de " + hIni + " - " + hEnd);

        tvAddress.setText(intent.getStringExtra(DetailsExtraDataKeys.KEY_ADDRESS));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        double lat = getIntent().getDoubleExtra(DetailsExtraDataKeys.KEY_LAT, 0);
        double lon = getIntent().getDoubleExtra(DetailsExtraDataKeys.KEY_LON, 0);
        String name = getIntent().getStringExtra(DetailsExtraDataKeys.KEY_NAME);
        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .title(name));
        LatLngBounds bounds = new LatLngBounds(new LatLng(lat - 0.005, lon - 0.005), new LatLng(lat + 0.005, lon + 0.005));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));

    }
}

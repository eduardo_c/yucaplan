package com.yucaplan.eduardo.yucaplan.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Created by mvarguez on 14/10/2016.
 */

public class SharedPreferencesManager {
    private static final String USER_ID_KEY = "userid";
    private static final String ACCESS_TOKEN_KEY = "facebookAccessToken";
    private static final String USER_NAME_KEY = "userName";
    private static final String USER_EMAIL_KEY = "userEmail";
    private static final String USER_PICTURE_URL_KEY = "userPictureUrl";

    private static SharedPreferences getSharedPreferences(Context ctx){
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserId(Context ctx, String userId){
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_ID_KEY, userId);
        editor.commit();
    }

    public static void setAccessToken(Context ctx, String token){
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(ACCESS_TOKEN_KEY, token);
        editor.commit();
    }

    public static void setUserName(Context ctx, String name){
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_NAME_KEY, name);
        editor.commit();
    }

    public static void setUserEmail(Context ctx, String email){
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_EMAIL_KEY, email);
        editor.commit();
    }

    public static void setUserPictureUrl(Context ctx, String url){
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_PICTURE_URL_KEY, url);
        editor.commit();
    }

    public static String getUserId(Context ctx){
        return getSharedPreferences(ctx).getString(USER_ID_KEY, null);
    }

    public static String getAccessToken(Context ctx){
        return getSharedPreferences(ctx).getString(ACCESS_TOKEN_KEY, null);
    }

    public static String getUserName(Context ctx){
        return getSharedPreferences(ctx).getString(USER_NAME_KEY, null);
    }

    public static String getUserEmail(Context ctx){
        return getSharedPreferences(ctx).getString(USER_EMAIL_KEY, null);
    }

    public static String getUserProfilePicture(Context ctx){
        return getSharedPreferences(ctx).getString(USER_PICTURE_URL_KEY, null);
    }
}

package com.yucaplan.eduardo.yucaplan.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.yucaplan.eduardo.yucaplan.R;
import com.yucaplan.eduardo.yucaplan.preferences.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final String LOG_TAG = LoginActivity.class.getName();

    private LoginButton mLoginButton;
    private CallbackManager mCallbackManager;
    private Context mContext = LoginActivity.this;
    private ProgressDialog mProgressDialog;
    private EditText etEmail;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        if(getSupportActionBar() != null)
            getSupportActionBar().hide();

        findViewById(R.id.button3).setOnClickListener((View.OnClickListener) mContext);

        etEmail = (EditText) findViewById(R.id.editText);
        etPassword = (EditText) findViewById(R.id.editText2);

        mCallbackManager = CallbackManager.Factory.create();
        mLoginButton = (LoginButton) findViewById(R.id.login_button);
        mLoginButton.setReadPermissions(Arrays.asList("public_profile", "email"));

        mLoginButton.setOnClickListener(LoginActivity.this);

        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.d(LOG_TAG, response.toString());
                                SharedPreferencesManager.setUserPictureUrl(mContext, "https://graph.facebook.com/" + loginResult.getAccessToken().getUserId() + "/picture?type=large");
                                SharedPreferencesManager.setUserId(mContext, loginResult.getAccessToken().getUserId());
                                SharedPreferencesManager.setAccessToken(mContext, loginResult.getAccessToken().toString());
                                try {
                                    SharedPreferencesManager.setUserName(mContext, object.getString("name"));
                                    SharedPreferencesManager.setUserEmail(mContext, object.getString("email"));
                                    saveFacebookUserInServer(loginResult, object);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                setResult(3);
                                Intent intent = new Intent(mContext, MainActivity.class);
                                startActivity(intent);
                                LoginActivity.this.finish();
                                mProgressDialog.dismiss();
                            }
                        }
                );
                Bundle parameters = new Bundle();
                parameters.putString("fields", "name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(LOG_TAG, "Login with facebook canceled");
                mProgressDialog.dismiss();
                Toast.makeText(LoginActivity.this, getString(R.string.login_canceled), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(LOG_TAG, error.getCause().toString());
                mProgressDialog.dismiss();
                Toast.makeText(LoginActivity.this, getString(R.string.login_canceled), Toast.LENGTH_LONG).show();
            }
        });

        topLevelFocus(mContext);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.login_button:
                mProgressDialog = new ProgressDialog(LoginActivity.this);
                mProgressDialog.setMessage(getString(R.string.login_progress_bar_message));
                mProgressDialog.show();
                break;
            case R.id.button3:
                mProgressDialog = new ProgressDialog(LoginActivity.this);
                mProgressDialog.setMessage(getString(R.string.login_progress_bar_message_mail));
                mProgressDialog.show();
                String mail = etEmail.getText().toString();
                final String password = etPassword.getText().toString();

                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo("email", mail);
                query.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> list, ParseException e) {
                        if(e == null && list != null){
                            if(list.size() == 1){
                                String name = list.get(0).getUsername();
                                attemptLogIn(name, password);
                            }
                            else {
                                mProgressDialog.dismiss();
                                Toast.makeText(mContext, getString(R.string.error_not_registered), Toast.LENGTH_LONG).show();
                            }
                        }
                        else if(e != null) {
                            mProgressDialog.dismiss();
                            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        else {
                            mProgressDialog.dismiss();
                        }
                    }
                });
        }
    }

    private void attemptLogIn(String userName, String password){
        ParseUser.logInInBackground(userName, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if(parseUser != null){
                    SharedPreferencesManager.setUserEmail(mContext, parseUser.getEmail());
                    SharedPreferencesManager.setUserName(mContext, parseUser.getUsername());
                    SharedPreferencesManager.setUserId(mContext, parseUser.getObjectId());
                    setResult(3);
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }
                else {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        setResult(3);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        setResult(3);
        super.onDestroy();
    }

    /**
     * Guarda los datos de un usuario de facebook en el servidor de parse en caso de que
     * sea la primera vez que hace login con facebook
     * @param loginResult
     * @param jsonObject
     * @throws JSONException
     */
    private void saveFacebookUserInServer(final LoginResult loginResult, final JSONObject jsonObject) throws JSONException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("FacebookUser");
        query.whereEqualTo("email", jsonObject.getString("email"));
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    if(list.isEmpty()){
                        ParseObject facebookUser = new ParseObject("FacebookUser");
                        facebookUser.put("facebookId", loginResult.getAccessToken().getUserId());
                        try {
                            facebookUser.put("name", jsonObject.getString("name"));
                            facebookUser.put("email", jsonObject.getString("email"));
                        }
                        catch (JSONException error){
                            error.printStackTrace();
                        }
                        facebookUser.saveInBackground();
                    }
                }
                else{
                    Log.e(LOG_TAG, "error: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    public static void topLevelFocus(Context context){
        if(Activity.class.isAssignableFrom(context.getClass())){
            ViewGroup tlView = (ViewGroup) ((Activity) context).getWindow().getDecorView();
            if(tlView!=null){
                tlView.setFocusable(true);
                tlView.setFocusableInTouchMode(true);
                tlView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
            }
        }
    }
}

package com.yucaplan.eduardo.yucaplan.utils;

import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.parse.Parse;
import com.parse.ParseUser;
import com.yucaplan.eduardo.yucaplan.R;
import com.yucaplan.eduardo.yucaplan.preferences.SharedPreferencesManager;

/**
 * Created by eduardo on 16/10/2016.
 */
public class SessionManager {

    public static boolean isUserLogged(Context ctx){
        if(SharedPreferencesManager.getUserEmail(ctx) == null){
            return false;
        }
        else{
            return true;
        }
    }

    public static void logout(Context ctx, ParseUser currentUser){
        if(currentUser != null)
            ParseUser.logOut();
        FacebookSdk.sdkInitialize(ctx);
        LoginManager.getInstance().logOut();
        SharedPreferencesManager.setUserId(ctx, null);
        SharedPreferencesManager.setAccessToken(ctx, null);
        SharedPreferencesManager.setUserName(ctx, null);
        SharedPreferencesManager.setUserEmail(ctx, null);
        SharedPreferencesManager.setUserPictureUrl(ctx, null);
    }
}

package com.yucaplan.eduardo.yucaplan.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.parse.Parse;
import com.yucaplan.eduardo.yucaplan.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            Parse.initialize(new Parse.Configuration.Builder(this)
                    .applicationId(getString(R.string.PARSE_APP_ID))
                    .clientKey(getString(R.string.PARSE_CLIENT_KEY))
                    .server(getString(R.string.PARSE_SERVER)).build());
        }
        catch (IllegalStateException e){
            e.printStackTrace();
        }

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
